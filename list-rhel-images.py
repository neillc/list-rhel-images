#!/bin/env python3
"""
A script to list the available RHEL images available to download.

Requires a token (see https://access.redhat.com/management/api to generate a token)

By default, this will list all the available images for RHEL 8.4 on the x86_64 architecture in json format.



"""
import argparse
import json
import sys

from tabulate import tabulate
import requests
from yaml import dump


def parse_arguments():
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-a",
        "--architecture",
        type=str,
        default="x86_64",
        help="Architecture. Default is x86_64",
    )

    # noinspection PyTypeChecker
    parser.add_argument(
        "-t",
        "--token",
        type=str,
        help="".join(
            [
                "The offline token used to generate the bearer token. "
                "Either this or --token-file must be specified.",
            ]
        ),
    )

    parser.add_argument(
        "--token-file",
        type=open,
        help="The file to read the token from. Either this or --token must be specified.",
    )

    parser.add_argument(
        "-v",
        "--version",
        type=str,
        default="8.4",
        help="Version. Default is 8.4",
    )

    parser.add_argument("-n", "--name", type=str, help="Filter names")

    parser.add_argument(
        "-f",
        "--format",
        choices=["json", "table", "yaml"],
        type=str,
        default="yaml",
        help="Output format, one of yaml, json or table. Default is yaml",
    )

    args = parser.parse_args()

    if not (args.token or args.token_file):
        sys.stderr.write(
            "Either --token-file or --token/-t must be supplied"
        )
        sys.exit(1)

    if args.token_file:
        args.token = args.token_file.read()

    return args


def get_token(args):
    req = requests.post(
        "https://sso.redhat.com/auth/realms/redhat-external/protocol/openid-connect/token",
        data={
            "grant_type": "refresh_token",
            "client_id": "rhsm-api",
            "refresh_token": args.token,
        },
        timeout=120,
        )

    result = req.json()
    if not req.ok:
        sys.stderr.write(f"Could not get access token - {result['error']}: {result['error_description']}")
        sys.exit(1)

    try:
        token = req.json()["access_token"]
    except KeyError:
        sys.stderr.write(f"Could not get access token - token not found in result")
        sys.exit(1)

    return token

def main():
    """Main function."""
    args = parse_arguments()
    token = get_token(args)

    url = f"https://api.access.redhat.com/management/v1/images/rhel/{args.version}/{args.architecture}"

    req = requests.get(
        url,
        headers={"Authorization": "Bearer " + token},
        timeout=120,
    )

    if req.ok:
        if args.name:
            images = [ i  for i in req.json()["body"] if args.name in i["imageName"] ]
        else:
            images = req.json()["body"]



        if args.format == "table":
            table = [
                [
                    "Name",
                    "Architecture",
                    "Checksum",
                    "URI",
                    "Filename",
                    "Date",
                ]
            ]
            for i in images:
                table.append(
                    [
                        i["imageName"],
                        i["arch"],
                        i["checksum"],
                        i["downloadHref"],
                        i["filename"],
                        i["datePublished"],
                    ]
                )
            print(tabulate(table, headers="firstrow"))
        elif args.format == "json":
            print(json.dumps(images, indent=4))
        else:
            print(dump(images))
    else:
        sys.stderr.write("Could not get image list\n")
        sys.stderr.write(f"{req.json()['error']['code']} - {req.json()['error']['message']}\n")
        sys.exit(1)

if __name__ == "__main__":
    main()
