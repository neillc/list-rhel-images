#!/bin/env python3
"""
A script to list the available RHEL images available to download.

Requires a token (see https://access.redhat.com/management/api to generate a token)

By default, this will list all the available images for RHEL 8.4 on the x86_64 architecture in json format.


"""
import argparse
import sys

import requests
from yaml import load
try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader


def parse_arguments():
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser()

    # noinspection PyTypeChecker
    parser.add_argument(
        "-t",
        "--token",
        type=str,
        help="".join(
            [
                "The offline token used to generate the bearer token. "
                "Either this or --token-file must be specified.",
            ]
        ),
    )

    parser.add_argument(
        "--token-file",
        type=open,
        help="The file to read the token from. Either this or --token must be specified.",
    )

    parser.add_argument(
        "--images-file",
        type=open,
        help="The file to read the list of images to download. Must be valid yaml. " 
             "Use list-rhel-images to generate such a file.",
        required=True
    )

    args = parser.parse_args()

    if not (args.token or args.token_file):
        sys.stderr.write(
            "Either --token-file or --token/-t must be supplied"
        )
        sys.exit(1)

    if args.token_file:
        args.token = args.token_file.read()

    return args


def get_token(args):
    req = requests.post(
        "https://sso.redhat.com/auth/realms/redhat-external/protocol/openid-connect/token",
        data={
            "grant_type": "refresh_token",
            "client_id": "rhsm-api",
            "refresh_token": args.token,
        },
        timeout=120,
        )

    result = req.json()
    if not req.ok:
        sys.stderr.write(f"Could not get access token - {result['error']}: {result['error_description']}")
        sys.exit(1)

    try:
        token = req.json()["access_token"]
    except KeyError:
        sys.stderr.write(f"Could not get access token - token not found in result")
        sys.exit(1)

    return token

def download_image(image):
    with requests.get(image["downloadHref"], stream=True) as r:
        r.raise_for_status()
        bytes_downloaded = 0
        with open(image["filename"], 'wb') as f:
            for chunk in r.iter_content(chunk_size=8192):
                f.write(chunk)
                bytes_downloaded += 8192
                print(f"{bytes_downloaded}")


def main():
    """Main function."""
    args = parse_arguments()
    token = get_token(args)

    images = load(args.images_file, Loader=Loader)

    for image in images:
        print(image["downloadHref"])
        print(image["filename"])
        download_image(image)


if __name__ == "__main__":
    main()
