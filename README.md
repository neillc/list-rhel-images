# List RHEL Images

This is a script to list available RHEL images for download.

Requires an offline token (see https://access.redhat.com/management/api) to generate one.

./list-rhel-images.py --help
usage: list-rhel-images.py [-h] [-t TOKEN] [--token-file TOKEN_FILE] [-a ARCHITECTURE] [-v VERSION] [-f {json,table}]

options:
  -h, --help            show this help message and exit
  -t TOKEN, --token TOKEN
                        The offline token used to generate the bearer token
  --token-file TOKEN_FILE
  -a ARCHITECTURE, --architecture ARCHITECTURE
                        Architecture
  -v VERSION, --version VERSION
                        Version
  -f {json,table}, --format {json,table}
                        Output format, one of json or table
